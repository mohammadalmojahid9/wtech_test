import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wtechtest/pages/account/account_page.dart';
import 'package:wtechtest/pages/add/add_page.dart';
import 'package:wtechtest/pages/home/home_page.dart';
import 'package:wtechtest/pages/profile/profile_page.dart';
import 'package:wtechtest/pages/wallet/wallwt_page.dart';
import 'package:wtechtest/utils/conts.dart';

import 'dashboard_controller.dart';

class DashboardPage extends StatelessWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<DashboardController>(builder: (controller) {
      return Scaffold(
        body: SafeArea(
          child: IndexedStack(
            index: controller.tabIndex,
            children: const [
              HomePage(),
              WalletPage(),
              AddPage(),
              AccountPage(),
              ProfilePage(),
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          selectedIconTheme: const IconThemeData(
            color: Colors.black,
            size: 25,
          ),
          type: BottomNavigationBarType.fixed,
          backgroundColor: bgColor,
          elevation: 0.0,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          unselectedItemColor: blackColor,
          selectedItemColor: blueColor,
          onTap: controller.changeTabIngex,
          currentIndex: controller.tabIndex,

          items: [
            _bottomNavigationBarItem(icon: CupertinoIcons.home),
            _bottomNavigationBarItem(icon: CupertinoIcons.creditcard),
              BottomNavigationBarItem(
              icon: Container(

                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: blueColor,

                  ),

                  child: const Padding(
                padding: EdgeInsets.all(8.0),
                child: Icon(Icons.add , size: 25,color: Colors.white,),
              )),
              label: "add",
              backgroundColor: blueColor,
            ),
            _bottomNavigationBarItem(icon: CupertinoIcons.strikethrough),
            _bottomNavigationBarItem(icon: Icons.account_circle_outlined),
          ],
        ),
      );
    });
  }

  _bottomNavigationBarItem({
    required IconData icon,
  }) {
    return BottomNavigationBarItem(
      icon: Icon(icon),
      label: "home",
    );
  }
}
