import 'package:flutter/material.dart';

class CardShadow extends StatelessWidget {
  final Widget child;
  final double? width;
  final double? height;
  const CardShadow({
    Key? key,
    required this.child, this.width, this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(.0),
      child: Container(
        width:width ,
        height:height ,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: const BorderRadius.all(
            Radius.circular(30),
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.shade400,

              blurRadius: 6.0,
              offset: const Offset(0, 1), // changes position of shadow
            ),
          ],
        ),
        child: child,
      ),
    );
  }
}
