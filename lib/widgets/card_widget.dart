import 'package:flutter/material.dart';
import 'package:wtechtest/utils/conts.dart';
import 'package:wtechtest/widgets/card_shadow.dart';

class CardWidget extends StatelessWidget {
  final String image, title, subtitle, price;
  final Color colorBgImage;
  const CardWidget(
      {Key? key,
      required this.image,
      required this.title,
      required this.subtitle,
      required this.price,
      required this.colorBgImage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0 , vertical: 10.0),
      child: CardShadow(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded (
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Container(
                        decoration:
                            BoxDecoration(
                                color: colorBgImage,
                                borderRadius: BorderRadius.circular(15.0)),
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Center(
                            child: Image(
                              width: 20,
                              height: 20,
                              image: AssetImage(image),
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(width: 12.0,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(title , style:titleBlackTextStyle ,),
                        const SizedBox(height: 5.0,),
                        Text(subtitle , style: littleGrayTextStyle,softWrap: true,overflow:TextOverflow.ellipsis ,maxLines: 2,),
                      ],
                    ),

                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Text("\$$price"  ,style: subTitleBlackBoldTextStyle,),
              )
            ],
          ),
        ),
      ),
    );
  }
}
