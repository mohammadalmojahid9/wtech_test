import 'package:get/get.dart';
import 'package:wtechtest/pages/account/account_controller.dart';
import 'package:wtechtest/pages/add/add_controller.dart';
import 'package:wtechtest/pages/dashboard/dashboard_controller.dart';
import 'package:wtechtest/pages/home/home_controller.dart';
import 'package:wtechtest/pages/profile/profile_controller.dart';
import 'package:wtechtest/pages/wallet/wallet_controller.dart';



class DashboardBinding extends Bindings
{
  @override
  void dependencies() {
    Get.lazyPut<DashboardController>(() => DashboardController());
    Get.lazyPut<HomeController>(() => HomeController());
    Get.lazyPut<AccountController>(() => AccountController());
    Get.lazyPut<ProfileController>(() => ProfileController());
    Get.lazyPut<WalletController>(() => WalletController());
    Get.lazyPut<AddController>(() => AddController());
  }

}