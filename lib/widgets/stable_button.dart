import 'package:flutter/material.dart';

class ButtonStable extends StatelessWidget {
  final Color colorButton, textcolor;
  final String text;
  final VoidCallback onClicked;

  const ButtonStable({
    Key? key,
    required this.text,
    required this.onClicked,
    this.colorButton = const Color(0xff3E4685),
    required this.textcolor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.only(right: 15.0 , left: 15.0, top: 8.0 , bottom: 15.0),
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(colorButton),
            minimumSize: MaterialStateProperty.all(
                Size(MediaQuery.of(context).size.height / 2, 40)),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            )),
          ),
          child: FittedBox(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 12.0),
                child: Text(
                  text,
                  style: TextStyle(
                      color: textcolor,
                      fontSize: 16,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
          ),
          onPressed: onClicked,
        ),
      );
}

class HeaderWidget extends StatelessWidget {
  final Widget child;

  const HeaderWidget({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 8),
          child,
        ],
      );
}
