import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wtechtest/utils/conts.dart';
import 'package:get/get.dart';
import 'package:wtechtest/widgets/card_widget.dart';
import 'package:wtechtest/widgets/stable_button.dart';
class RecentTransaction extends StatelessWidget {
  const RecentTransaction({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        height: size.height,
        width: size.width,
        decoration: const BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.cover, image: AssetImage("assets/images/bg.png"))),
        child: SafeArea(
          child: Column(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        IconButton(
                          onPressed: () {Get.back();},
                          icon: const Icon(Icons.arrow_back_ios_new_outlined),
                          color: Colors.black,
                        ),
                        IconButton(
                          onPressed: () {},
                          icon: const Icon(CupertinoIcons.search),
                          color: Colors.black,
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: const [
                          Text(
                            "Recent Transactions",
                            style: headerBlueTextStyle,
                          ),
                          Text(
                            "see all",
                            style: smallBlackTextStyle,
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0 , vertical: 20.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          _buttonPress(
                              title: "All ",
                              textColor: Colors.white,
                              bgColor: blueColor),
                          const SizedBox(
                            width: 15.0,
                          ),
                          _buttonPress(
                              title: "Income",
                              textColor: blackColor,
                              bgColor: Colors.white),
                          const SizedBox(
                            width: 15.0,
                          ),
                          _buttonPress(
                              title: "Expense",
                              textColor: blackColor,
                              bgColor: Colors.white),
                        ],
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20.0 , vertical: 10.0),
                      child: Text(
                        "Today",
                        style: headerBlueTextStyle,
                      ),
                    ),
                    const CardWidget(
                      image: "assets/images/email.png",
                      title: "Payment",
                      subtitle: "Payment from Angrea",
                      colorBgImage: Color(0xffe9eafc),
                      price: "300",
                    ),
                  ],
                ),
              ),

              ButtonStable(
                onClicked: () {},
                text: "See Details",
                textcolor: Colors.white,
                colorButton: const Color(0xff3E4685),
              )
            ],
          ),
        ),
      ),
    );
  }
}

Widget _buttonPress(
    {required String title, required Color textColor, required Color bgColor}) {
  return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(15.0),
      color: bgColor,
    ),
    child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0, vertical: 5.0),
      child: Text(
        title,
        style: TextStyle(
            color: textColor, fontWeight: FontWeight.w400, fontSize: 14),
      ),
    ),
  );
}
