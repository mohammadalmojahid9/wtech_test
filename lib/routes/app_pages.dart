import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wtechtest/pages/account/account_binding.dart';
import 'package:wtechtest/pages/account/account_page.dart';
import 'package:wtechtest/pages/add/add_binding.dart';
import 'package:wtechtest/pages/add/add_page.dart';
import 'package:wtechtest/pages/dashboard/dashboard_binding.dart';
import 'package:wtechtest/pages/dashboard/dashboard_page.dart';
import 'package:wtechtest/pages/home/home_binding.dart';
import 'package:wtechtest/pages/home/home_page.dart';
import 'package:wtechtest/pages/profile/profile_binding.dart';
import 'package:wtechtest/pages/profile/profile_page.dart';
import 'package:wtechtest/pages/wallet/wallet_binding.dart';
import 'package:wtechtest/pages/wallet/wallwt_page.dart';


import 'app_routes.dart';
class  AppPages{
  static var list = [
    GetPage(
        name: AppRoutes.DASHBOARD,
        page: ()=> const DashboardPage() ,
        binding:DashboardBinding ()
    ),
    GetPage(
        name: AppRoutes.HOME,
        page: ()=> const HomePage() ,
       binding:HomeBinding()
    ),
    GetPage(
        name: AppRoutes.WALLET,
        page: ()=> const WalletPage() ,
        binding:WalletBinding()
    ),
    GetPage(
        name: AppRoutes.ACCOUNT,
        page: ()=> const AccountPage() ,
        binding:AccountBinding()
    ),
    GetPage(
        name: AppRoutes.ADD,
        page: ()=> const AddPage() ,
        binding:AddBinding()
    ),
    GetPage(
        name: AppRoutes.PROFILE,
        page: ()=> const ProfilePage() ,
        binding:ProfileBinding()
    ),
  ];
}