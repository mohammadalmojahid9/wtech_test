import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wtechtest/pages/home/home_controller.dart';
import 'package:wtechtest/routes/app_routes.dart';
import 'package:wtechtest/screens/recent_transactions_screeen.dart';
import 'package:wtechtest/utils/conts.dart';
import 'package:wtechtest/widgets/card_widget.dart';
import 'package:wtechtest/widgets/profile_card_widget.dart';
import 'package:date_format/date_format.dart';
class HomePage extends GetView<HomeController> {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
      children: [
            const ProfileCardWidget(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0 , vertical: 20.0),
              child: Row(
                children: [
                  Expanded(
                      child: Row(
                    children: const [
                      Text(
                        "Overview",
                        style: headerBlueTextStyle,
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      Icon(
                        Icons.add_alert_rounded,
                        size: 20,
                        color: blueColor,
                      )
                    ],
                  )),
                  Text(formatDate(DateTime.now() , [yy, '-', M, '-', d]) )
                ],
              ),
            ) ,
            InkWell(
              onTap: ()=> Get.to(()=> const RecentTransaction()),
              child: const CardWidget(image: "assets/images/arrow_up.png",
              price: "150",
                colorBgImage: bgColor,
                subtitle: "Sending Payment to Clients",
                title: "Sent",
              ),
            ),
          InkWell(
            onTap: ()=> Get.to(()=> const RecentTransaction()),
              child: const CardWidget(image: "assets/images/arrow_down.png",
              price: "150",
                colorBgImage: bgColor,
                subtitle: "Receive Salary from company",
                title: "Receive",
              ),
            ),
          InkWell(
            onTap: ()=> Get.to(()=> const RecentTransaction()),
              child: const CardWidget(image: "assets/images/\$.png",
              price: "400",
                colorBgImage: bgColor,
                subtitle: "Loan for the Car",
                title: "Loan",
              ),
            ),
      ],
    ),
          ),
        ));
  }
}
