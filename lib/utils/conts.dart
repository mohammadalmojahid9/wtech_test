import 'package:flutter/material.dart';

const bgColor = Color(0xfff3f8fe);
const blueColor = Color(0xff3E4685);
const blackColor = Color(0xff0d0a0f);
const grayColor = Color(0xffbbb3bf);

////////blue
const headerBlueTextStyle = TextStyle(
    color: blueColor,
    fontSize: 22,
    fontWeight: FontWeight.w800,
    letterSpacing: 1.0);
const titleBlueTextStyle = TextStyle(
  color: blueColor,
  fontSize: 18,
  fontWeight: FontWeight.w600,
);
const titleBlueLightTextStyle = TextStyle(
  color: blueColor,
  fontSize: 15,
  fontWeight: FontWeight.w400,
);

/////////gray

const littleGrayTextStyle = TextStyle(
    color: grayColor,
    fontSize: 12,
    fontWeight: FontWeight.w400,
    letterSpacing: -0.9);

////////// Black Color
const smallBlackTextStyle = TextStyle(
  color: blackColor,
  fontSize: 16,
  fontWeight: FontWeight.w400,
);

const titleBlackTextStyle = TextStyle(
  color: blackColor,
  fontSize: 18,
  fontWeight: FontWeight.w600,
);

const subTitleBlackTextStyle = TextStyle(
  color: blackColor,
  fontSize: 12,
  fontWeight: FontWeight.w400,
);
const subTitleBlackBoldTextStyle = TextStyle(
  color: blackColor,
  fontSize: 16,
  fontWeight: FontWeight.bold,
);
