import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wtechtest/routes/app_pages.dart';
import 'package:wtechtest/routes/app_routes.dart';
import 'package:wtechtest/screens/authintication/login/login_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
       themeMode:ThemeMode.light,

      debugShowCheckedModeBanner: false,
      title: 'WTech',
      home: const LoginScreen(),
      theme: ThemeData(
        scaffoldBackgroundColor:const Color(0xfff3f8fe),
        primarySwatch: Colors.blue,
      ),
      getPages: AppPages.list,


    );
  }
}

