import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:wtechtest/utils/conts.dart';

import 'card_shadow.dart';

class TextFieldWidget extends StatefulWidget {
  final String hintText;
  final IconData? icons;
  final String? Function(String?)? validator;
  final String? Function(String?)? onSave;
  final String? Function(String?)? onchange;
  final Void Function()? onTap;
  final bool isRead;
  final bool autoFocus;
  final int? maxLine;
  final bool isIcon;
  final String title;
  final Color? borderColor;
  final TextInputType? typeInput;

  final TextEditingController? controller;

  final bool? isCardShadow;

  const TextFieldWidget({
    Key? key,
    required this.hintText,
    required this.icons,
    required this.validator,
    required this.controller,
    this.isRead = false,
    this.autoFocus = false,
    required this.maxLine,
      this.onTap,
    required this.title,
    required this.typeInput,
    this.isIcon = false,
    this.borderColor = Colors.grey,

    required this.onSave,
    required this.onchange,

    this.isCardShadow = true,
  }) : super(key: key);

  @override
  _TextFieldWidgetState createState() => _TextFieldWidgetState();
}

class _TextFieldWidgetState extends State<TextFieldWidget> {
  @override
  Widget build(BuildContext context) {
    return CardShadow(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0 , vertical: 10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: Text(
                widget.title,
                style: const TextStyle(
                  color: Color(0xff7b797c),
                  fontSize: 16,
                ),
              ),
            ),

            TextFormField(
              onChanged: widget.onchange,
              onSaved: widget.onSave,
              scrollPadding: const EdgeInsets.only(bottom: 40),
              autofocus: widget.autoFocus,
              onTap: widget.onTap,
              readOnly: widget.isRead,
              maxLines: widget.maxLine,
              controller: widget.controller,
              keyboardType: widget.typeInput,
              validator: widget.validator,
              decoration: InputDecoration(
                  contentPadding: const EdgeInsets.symmetric(
                      horizontal: 12.0, vertical: 12.0),
                  fillColor: Colors.white,

                  filled: true,
                  prefixIcon: (widget.isIcon)
                      ? Icon(
                          widget.icons,
                          color: blackColor,
                        )
                      : null,
                  enabledBorder:  InputBorder.none,
                  focusedBorder:InputBorder.none   ,
                hintText: widget.hintText,
                  hintStyle: const TextStyle(
                      color: grayColor,
                      fontSize: 16,
                      fontWeight: FontWeight.w600),
                  border:InputBorder.none,)
            ),
          ],
        ),
      ),
    );
  }
}
