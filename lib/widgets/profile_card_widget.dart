import 'package:flutter/material.dart';
import 'package:wtechtest/utils/conts.dart';
import 'package:wtechtest/widgets/card_shadow.dart';

class ProfileCardWidget extends StatelessWidget {
  const ProfileCardWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
      child: CardShadow(
          height: size.width * 0.8,
          width: size.width,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0 , vertical: 15.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: const [
                    Image(
                      image: AssetImage(
                        "assets/images/line.png",
                      ),
                      width: 20,
                      height: 20,
                    ),
                    Image(
                      image: AssetImage("assets/images/circle .png"),
                      width: 15,
                      height: 15,
                    ),
                  ],
                ),

                Column(
                  children:  const [
                    Center(
                      child: CircleAvatar(
                        backgroundImage: AssetImage("assets/images/profile.jpg"),
                        radius: 45,

                      ),
                    ),
                      SizedBox(
                      height: 15.0,
                    ),
                    Text(
                      "Hira Riaz",
                      style: headerBlueTextStyle,
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    Text(
                      "UX/UI Designer",
                      style: subTitleBlackTextStyle,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 5.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const SizedBox(width: 10.0,),
                    widgetPriceTitle(title: "income", price: '8900'),
                    widgetPip(),
                    widgetPriceTitle(title: "Expenses", price: '5500'),
                    widgetPip(),
                    widgetPriceTitle(title: "Loan", price: '890'),
                    const SizedBox(width: 10.0,),
                  ],
                ),
                const SizedBox(
                  height: 5.0,
                ),
              ],
            ),
          )),
    );
  }
}

Widget widgetPriceTitle({required String title, required String price}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Text(
        "\$$price",
        style: titleBlueLightTextStyle,
      ),
      const SizedBox(
        height: 10.0,
      ),
      Text(
        title,
        style: subTitleBlackTextStyle,
      ),
    ],
  );
}

Container widgetPip() {
  return Container(
    height: 50,
    width: 1,
    color: grayColor,
  );
}
