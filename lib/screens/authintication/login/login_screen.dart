import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wtechtest/routes/app_routes.dart';
import 'package:wtechtest/utils/conts.dart';
import 'package:wtechtest/widgets/stable_button.dart';
import 'package:wtechtest/widgets/text_faild.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController emailTextEditingController =
      TextEditingController();
  final TextEditingController passwordTextEditingController =
      TextEditingController();
  bool _obscureText = true;
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SizedBox(
        height: size.height,
        width: size.width,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Spacer(),
              const CircleAvatar(
                backgroundColor: bgColor,
                backgroundImage: AssetImage("assets/images/logo.png"),
                radius: 55,
              ),
              const Spacer(),
              TextFieldWidget(
                validator: (val) {},
                maxLine: 1,
                typeInput: TextInputType.emailAddress,
                onchange: (val) {},
                onSave: (val) {},
                controller: emailTextEditingController,
                title: "Email Address",
                hintText: "example@gmail.com",
                icons: Icons.email,
                isRead: false,
                isIcon: true,
                autoFocus: false,
              ),
              const SizedBox(
                height: 20.0,
              ),
              Stack(
                children: [
                  TextFieldWidget(
                    validator: (val) {},
                    maxLine: 1,
                    typeInput: TextInputType.visiblePassword,
                    onchange: (val) {},
                    onSave: (val) {},
                    controller: passwordTextEditingController,
                    title: "Password",
                    hintText: "123AZ*/",
                    icons: CupertinoIcons.lock,
                    isRead: false,
                    isIcon: true,
                    autoFocus: false,
                  ),
                  Positioned(
                    right: 0.0,
                    top: 0.0,
                    bottom: 0.0,
                    child: IconButton(
                        onPressed: () {
                          setState(() {
                            _obscureText = !_obscureText;
                          });
                        },
                        icon: Icon(_obscureText
                            ? Icons.visibility_outlined
                            : Icons.visibility_off_outlined)),
                  )
                ],
              ),
              const SizedBox(
                height: 20.0,
              ),
              ButtonStable(
                textcolor: Colors.white,
                text: "Login",
                onClicked: () {
                  Get.toNamed(AppRoutes.DASHBOARD);
                },
                colorButton: blueColor,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Text(
                      "Signup",
                      style: titleBlueLightTextStyle,
                    ),
                    Text(
                      "ForgetPassword",
                      style: titleBlueLightTextStyle,
                    ),
                  ],
                ),
              ),
              const Spacer(
                flex: 2,
              )
            ],
          ),
        ),
      ),
    );
  }
}
